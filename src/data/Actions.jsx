import Foto1 from "../assets/images/meme1.jpeg";
import Foto2 from "../assets/images/meme2.jpeg";
import Foto3 from "../assets/images/meme3.jpeg";
import Foto4 from "../assets/images/meme4.jpg";
import Foto5 from "../assets/images/meme5.jpg";
import Foto6 from "../assets/images/meme6.jpg";
import Foto7 from "../assets/images/meme7.jpg";
import Foto8 from "../assets/images/meme8.jpg";
import Foto9 from "../assets/images/meme9.jpg";
import Foto10 from '../assets/images/meme10.jpeg'

export let doing = [
  {
    msg: ["Estaba preparando una infusion", "¿Vos que hacias?"],
  },
  {
    msg: ["Hoy no estoy de humor.", "Mejor hablamos otro dia.."],
  },
  {
    msg: ["Estoy cocinando colas de rata y patas de rana"],
  },

  {
    msg: ["¿Queres saber algo de mi?", "Preguntame lo que quieras"],
  },
  {
    msg: ["¿Todavia seguis aca?", "Hoy no quiero hablar"],
  },
  {
    msg: ["Estoy por salir a volar un rato en mi escoba"],
  },
  {
    msg: ["Aca, esperando que me cuentes algo interesante"],
  },
];

export let aboutMe = [
  {
    msg: ["Ay, pero que aburrimiento, mejor elegí otra opcion"],
  },
  {
    msg: [
      "Soy una bruja relativamente buena",
      "Si te portas bien, te cuento algo mas",
    ],
  },
  {
    msg: [
      "No me gusta festejar Halloween",
      "¿Te parece algo para festejar que quemen gente en una hoguera?",
    ],
  },
  {
    msg: ["Algunos dias salgo a asustar personas", " Eso me divierte"],
  },
  {
    msg: [
      "Me gusta salir a volar en mi escoba",
      "Y tambien usar mi sombrero negro",
    ],
  },
];

export let meme = [
  {
    msg: [Foto1],
  },
  {
    msg: [Foto2],
  },
  {
    msg: [Foto3],
  },
  {
    msg: [Foto4],
  },
  {
    msg: [Foto5],
  },
  {
    msg: [Foto6],
  },
  {
    msg: [Foto7],
  },
  {
    msg: [Foto8],
  },
  {
    msg: [Foto9],
  },
  {
    msg: [Foto10],
  },
];
