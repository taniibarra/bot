import React, { useState, useEffect, useRef } from "react";
import "./Chat.css";
import BotItem from "./BotItem/BotItem";
import Fade from "react-reveal/Fade";
import UserItem from "./UserItem/UserItem";
import InputChat from "./InputChat/InputChat";
import Select from "./Select/Select";
import { doing, aboutMe, meme } from "../../data/Actions";

const Chat = () => {
  let idCounter = 0;

  const [msg, setMsg] = useState({});

  const [openSelect, setOpenSelect] = useState(false);

  const [chat, setChat] = useState([
    {
      id: 0,
      emitter: "Bot",
      msg: ["Hola! ", "¿Cómo es tu nombre?"],
    },
  ]);

  function firstResponse(name) {
    let newChat = {
      id: idCounter + 2,
      emitter: "Bot",
      msg: [
        "¡Mucho gusto, " + name + "!",
        "Mi nombre es Circe, soy un bot todavia en desarrollo",
        "Eso quiere decir que aun no estoy preparada para preguntas especificas",
        "¡Lo que no siginifica que no podamos interactuar!",
        "Haceme una pregunta de la lista, y con gusto respondo",
      ],
    };
    if (newChat) {
      setChat([...chat, newChat]);
    }
  }

  useEffect(() => {
    if (chat.length === 2) {
      setTimeout(() => firstResponse(msg.msg), 500);
      setMsg({ ...msg, msg: "" });
      setTimeout(() => setOpenSelect(true), 600);
    }
  }, [chat]); // ver si se agrega chat en el array

  function getMeMessage(value) {
    idCounter = idCounter + 1;
    setMsg({
      id: idCounter,
      emitter: "User",
      msg: value,
    });
  }

  function sendMessage(e) {
    e.preventDefault();
    setChat([...chat, msg]);
  }

  let options = [
    {
      id: "What are you doing?",
      text: "¿Que haces?",
    },
    {
      id: "Send me a meme",
      text: "Mandame un meme",
    },
    {
      id: "Tell me about you",
      text: "Contame sobre vos",
    },
  ];

  const [interactions, setInteractions] = useState([]);

  function handleSelectedOptions(value) {
    let result;

    switch (value) {
      case "What are you doing?":
        result = doing[Math.floor(Math.random() * doing.length)];
        if (result) {
          setInteractions([...interactions, result.msg]);
        }
        break;

      case "Tell me about you":
        result = aboutMe[Math.floor(Math.random() * aboutMe.length)];
        if (result) {
          setInteractions([...interactions, result.msg]);
        }
        break;

      case "Send me a meme":
        result = meme[Math.floor(Math.random() * meme.length)];
        if (result) {
          setInteractions([...interactions, result.msg]);
        }

      default:
        console.log("No hay valores");
    }
  }

  return (
    <div className="chatbot-chat-container">
      <div className="chatbot-chat-content">
        <div className="chatbot-chat">
          <div className="chatbot-chat-container-body">
            {chat.map((message, index) =>
              message.emitter === "Bot" ? (
                <Fade left>
                  <BotItem key={index} text={message.msg} />
                </Fade>
              ) : (
                <UserItem key={index} text={message.msg} />
              )
            )}
            {openSelect && (
              <Fade right>
                <Select
                  handleSelectedOptions={handleSelectedOptions}
                  options={options}
                />
              </Fade>
            )}
            {interactions.length > 0 &&
              interactions.map((interaction, index) => (
                <>
                  <Fade left>
                    <BotItem key={index} text={interaction}></BotItem>
                  </Fade>
                  <Fade right>
                    <Select
                      key={index}
                      handleSelectedOptions={handleSelectedOptions}
                      options={options}
                    />
                  </Fade>
                </>
              ))}
          </div>
          <div className="chatbot-chat-container-input">
            <InputChat
              chat={chat}
              msg={msg}
              getMeMessage={getMeMessage}
              sendMessage={sendMessage}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Chat;
