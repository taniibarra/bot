import React from "react";
import "./BotItem.css";
import botAvatar from "../../../assets/images/witch.png";

const BotItem = ({ text }) => {
  return (
    <div className="bot-item-container">
      <img src={botAvatar} alt="Avatar" className="bot-item-img" />
      <div className="bot-item-messages">
        {text.map((t, index) =>
          t.startsWith("/static") ? (
            <img key={index} src={t} alt="meme" className="memes-images" />
          ) : (
            <label key={index}> {t} </label>
          )
        )}
      </div>
    </div>
  );
};

export default BotItem;
