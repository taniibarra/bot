import React from "react";
import Lottie from "react-lottie";
import "./Witch.css";
import animationWitch from "./10662-witch.json";

const Witch = ({ history }) => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationWitch,
  };

  function handleonClick() {
    history.push("/chat");
  }

  return (
    <div onClick={handleonClick} className="witch-container">
      <Lottie options={defaultOptions} />
    </div>
  );
};

export default Witch;
