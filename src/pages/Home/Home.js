import React from "react";
import Witch from "./components/Witch/Witch";
import "./Home.css";
import Fade from "react-reveal/Fade";

const Home = ({ history }) => {
  return (
    <div className="home-container">
      <div className="home-content">
        <Witch history={history} />
        <div className="home-greeting">
          <Fade opposite>
            <h1>¡Hola!</h1>
          </Fade>
          <Fade opposite>
            <label> ¿Querés charlar? </label>
            <label> Click sobre mi para comenzar :) </label>
          </Fade>
        </div>
      </div>
    </div>
  );
};

export default Home;
